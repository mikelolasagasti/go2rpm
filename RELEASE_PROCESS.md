# Release process

1. Ensure `nox` is installed
1. Ensure the `origin` remote points to the upstream repo
1. Ensure `twine` is configured to upload to PyPI.
1. Run full test suite

    ```
    nox
    ```
1. Determine last version
    ```
    last_version=0.1.0
    version=0.2.0
    ```
1. Create changelog fragment

    ```
    git log --pretty='- %s' --reverse v${last_version}..HEAD | \
        grep -vEi 'nox|Post release|ci|lint' > RELEASE_FRAGMENT.md
    ```

    and organize changes in `RELEASE_FRAGMENT.md` under `### Added`, `### Changed`,
    `### Deprecated`, and `### Removed` headings as appropriate.
1. Create a version bump and build Python distributions

    ```
    nox -e bump -- ${version}
    ```
1. Upload distributions to PyPI

    ```
    nox -e publish
    ```
